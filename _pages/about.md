---
title: "关于我"
permalink: /about/
date: 2018-06-26T21:38:52+08:00
---

> 中山大学南方学院——柯根（网络与新媒体专业），正在学习课程：《网页设计与制作》、《信息可视化设计》

### 柯根

[柯根 中山大学南方学院网络与新媒体学生](http://nfunm037.gitee.io/resume/)

- 兴趣爱好：弹吉他、打篮球、做网页 等等
- 年龄：19
- 学术专长： 开放协作；界面研究；用户调研 等等
